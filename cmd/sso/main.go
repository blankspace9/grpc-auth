package main

import (
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/blankspace9/grpc-auth/internal/app"
	"gitlab.com/blankspace9/grpc-auth/internal/config"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

func main() {
	cfg := config.MustLoad()

	log := setupLogger(cfg.Env)

	log.Info("starting application", slog.String("env", cfg.Env), slog.Int("port", cfg.GRPC.Port))

	application := app.New(log, config.AppConfig{
		Tokens:       cfg.Tokens,
		GRPC:         cfg.GRPC,
		Verification: cfg.Verification,
		Storage:      cfg.Storage,
		EmailSender:  cfg.EmailSender,
		Login:        cfg.Login,
	})

	go application.GRPCServer.MustRun()
	go application.UserCleaner.Run()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

	sign := <-stop

	log.Info("stopping application", slog.String("signal", sign.String()))

	application.GRPCServer.Stop()
	application.UserCleaner.Stop()

	log.Info("application stopped")
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case envLocal:
		log = slog.New(
			slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envDev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return log
}
