# grpc-auth
Сервис написан с использованием чистой архитектуры. Был реализован Graceful Shutdown для корректного завершения работы.

## Getting started
* Запуск приложения
`go run cmd/sso/main.go --config=./config/local.yaml`
* Запуск мигратора
`go run cmd/migrator/main.go --migrations-path=./migrations`
* Запуск мигратора для тестов
`go run cmd/migrator/main.go --migrations-path=./tests/migrations --migrations-table=migrations_test`

## TODO
* Refactor to RBAC.
* Refactor IsAdmin, SetRole, UnsetRole, Login.
* Временная блокировка аккаунта после нескольких неудачных логинов.
* Refresh tokens.
* Unit-тесты.
* Функциональные тесты.

## Problems
* В Auth сервис прокидываются лишние параметры (CheckInterval и т.д.)
* Хранение секретного слова в базе данных.
* Передача секретного слова в модели App. Небезопасно из-за риска попасть в логи и т.д.
