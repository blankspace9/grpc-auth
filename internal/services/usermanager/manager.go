package usermanager

import (
	"context"
	"log/slog"
	"time"

	"gitlab.com/blankspace9/grpc-auth/internal/lib/logger/sl"
)

type Manager struct {
	log           *slog.Logger
	userProvider  userProvider
	checkInterval time.Duration
	userTTL       time.Duration
	stop          chan struct{}
}

type userProvider interface {
	DeleteExpired(ctx context.Context, userTTL time.Duration) error
}

func New(logger *slog.Logger, userProvider userProvider, checkInterval, userTTL time.Duration) *Manager {
	return &Manager{
		log:           logger,
		userProvider:  userProvider,
		checkInterval: checkInterval,
		userTTL:       userTTL,
		stop:          make(chan struct{}),
	}
}

func (m *Manager) Start() {
	const op = "regmanager.Start"

	log := m.log.With(slog.String("op", op))

	ticker := time.NewTicker(m.checkInterval)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			err := m.userProvider.DeleteExpired(context.TODO(), m.userTTL)
			if err != nil {
				log.Warn("delete expired users error", sl.Err(err))
			}
			log.Info("expired users deleted")
		case <-m.stop:
			return
		}
	}
}

func (m *Manager) Stop() {
	const op = "regmanager.Stop"

	log := m.log.With(slog.String("op", op))

	log.Info("Stopping inactive users manager")

	close(m.stop)
}
