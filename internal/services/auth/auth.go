package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/blankspace9/grpc-auth/internal/config"
	"gitlab.com/blankspace9/grpc-auth/internal/domain/models"
	"gitlab.com/blankspace9/grpc-auth/internal/lib/jwt"
	"gitlab.com/blankspace9/grpc-auth/internal/lib/logger/sl"
	"gitlab.com/blankspace9/grpc-auth/internal/lib/mailer"
	"gitlab.com/blankspace9/grpc-auth/internal/lib/verification"
	"gitlab.com/blankspace9/grpc-auth/internal/storage"
	"golang.org/x/crypto/bcrypt"
)

type Auth struct {
	log          *slog.Logger
	userProvider UserProvider
	appProvider  AppProvider
	linkProvider LinkProvider
	tokens       config.Tokens
	verification config.VerificationConfig
	mailer       config.EmailSender
	login        config.Login
}

type UserProvider interface {
	SaveUser(ctx context.Context, email string, passHash []byte) (uid int64, err error)
	User(ctx context.Context, email string) (models.User, error)
	UserByID(ctx context.Context, userID int64) (models.User, error)
	DeleteUser(ctx context.Context, email string) error
	VerifyUser(ctx context.Context, userID int64) error
	IncreaseLoginAttempts(ctx context.Context, email string) error
	ResetLoginAttempts(ctx context.Context, email string) error

	IsAdmin(ctx context.Context, userID int64) (bool, error)
	SetAdmin(ctx context.Context, userID int64) error
	UnsetAdmin(ctx context.Context, userID int64) error
}

type LinkProvider interface {
	RegisterLink(ctx context.Context, verificationToken string) (models.Link, error)
	CreateRegisterLink(ctx context.Context, userID int64, verificationToken string, linkTTL time.Duration) (int64, error)
	RefreshRegisterLink(ctx context.Context, userID int64, verificationToken string, linkTTL time.Duration) error
	DeleteRegisterLink(ctx context.Context, linkID int64) error
}

type AppProvider interface {
	App(ctx context.Context, appID int) (models.App, error)
}

var (
	ErrInvalidCredentials   = errors.New("invalid credentials")
	ErrInvalidUserID        = errors.New("invalid user ID")
	ErrUserExists           = errors.New("user already exists")
	ErrUserVerified         = errors.New("user already verified")
	ErrUserIsAlreadyAdmin   = errors.New("user is already admin")
	ErrConfirmationRequired = errors.New("confirmation required")

	ErrCaptchaRequired           = errors.New("captcha required")
	ErrCaptchaVerificationFailed = errors.New("captcha verification failed")

	ErrExpiredLink = errors.New("expired link")

	ErrInvalidAppID = errors.New("invalid app ID")
)

// New return a new instance of the Auth service
func New(log *slog.Logger, userProvider UserProvider, linkProvider LinkProvider, appProvider AppProvider, tokens config.Tokens, verification config.VerificationConfig, mailer config.EmailSender, login config.Login) *Auth {
	return &Auth{
		log:          log,
		userProvider: userProvider,
		linkProvider: linkProvider,
		appProvider:  appProvider,
		tokens:       tokens,
		verification: verification,
		mailer:       mailer,
		login:        login,
	}
}

// Login checks if user given credentials exists in the system and returns token.
//
// If user exists, but password is incorrect, returns error.
// If user doesn't exist, returns error.
func (a *Auth) Login(ctx context.Context, email string, password string, appID int) (string, string, error) {
	const op = "auth.Login"

	log := a.log.With(slog.String("op", op))

	log.Info("attempting to login user")

	user, err := a.userProvider.User(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(err))

			return "", "", fmt.Errorf("%s: %w", op, ErrInvalidCredentials)
		}

		a.log.Error("failed to get user", sl.Err(err))

		return "", "", fmt.Errorf("%s: %w", op, err)
	}

	if !user.Verified {
		a.log.Warn("confirmation required", sl.Err(ErrConfirmationRequired))

		return "", "", fmt.Errorf("%s: %w", op, ErrConfirmationRequired)
	}

	if err := bcrypt.CompareHashAndPassword(user.PassHash, []byte(password)); err != nil {
		a.log.Warn("invalid credentials", sl.Err(err))

		err = a.userProvider.IncreaseLoginAttempts(ctx, email)
		if err != nil {
			return "", "", fmt.Errorf("%s: %w", op, err)
		}

		if user.LoginAttempts >= a.login.MaxAttempts {
			a.log.Warn("captcha required", sl.Err(ErrCaptchaRequired))

			return "", "", fmt.Errorf("%s: %w", op, ErrCaptchaRequired)
		}

		return "", "", fmt.Errorf("%s: %w", op, ErrInvalidCredentials)
	}

	app, err := a.appProvider.App(ctx, appID)
	if err != nil {
		if errors.Is(err, storage.ErrAppNotFound) {
			log.Warn("app not found", sl.Err(err))

			return "", "", fmt.Errorf("%s: %w", op, ErrInvalidAppID)
		}

		a.log.Error("failed to get app", sl.Err(err))

		return "", "", fmt.Errorf("%s: %w", op, err)
	}

	err = a.userProvider.ResetLoginAttempts(ctx, email)
	if err != nil {
		return "", "", fmt.Errorf("%s: %w", op, err)
	}

	log.Info("user logged in successfully")

	accessToken, refreshToken, err := jwt.NewTokens(user, app, []string{"user_role"}, a.tokens.AccessTokenTTL)
	if err != nil {
		a.log.Error("failed to generate token", sl.Err(err))

		return "", "", fmt.Errorf("%s: %w", op, err)
	}

	return accessToken, refreshToken, nil
}

func (a *Auth) VerifyCaptcha(ctx context.Context, email, captchaResponse string) error {
	const op = "auth.Login"

	log := a.log.With(slog.String("op", op))

	log.Info("verifying captcha")

	params := url.Values{}
	params.Add("secret", a.login.CaptchaKey)
	params.Add("response", captchaResponse)

	u, err := url.Parse(a.login.CaptchaServerPath)
	if err != nil {
		a.log.Error("failed to parse url path", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}
	u.RawQuery = params.Encode()

	resp, err := http.Post(u.String(), "application/json", nil)
	if err != nil {
		a.log.Error("failed request to captcha server", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		a.log.Error("failed request to captcha server", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	var result struct {
		Success bool `json:"success"`
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		a.log.Error("failed parse captcha response", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	if !result.Success {
		a.log.Error("invalid captcha result", sl.Err(ErrCaptchaVerificationFailed))

		return fmt.Errorf("%s: %w", op, ErrCaptchaVerificationFailed)
	}

	err = a.userProvider.ResetLoginAttempts(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Error("user not found", sl.Err(ErrInvalidCredentials))

			return fmt.Errorf("%s: %w", op, ErrInvalidCredentials)
		}

		a.log.Error("failed reset login attempts", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

// RegisterNewUser registers new user in the system and return user ID.
//
// If user with given username already exists, return error.
func (a *Auth) RegisterNewUser(ctx context.Context, email, password, route string) (int64, error) {
	const op = "auth.RegisterNewUser"

	log := a.log.With(slog.String("op", op))

	log.Info("registering user")

	passHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("failed to generate password hash", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	id, err := a.userProvider.SaveUser(ctx, email, passHash)
	if err != nil {
		if errors.Is(err, storage.ErrUserExists) {
			log.Warn("user already exists", sl.Err(err))

			return 0, fmt.Errorf("%s: %w", op, ErrUserExists)
		}

		log.Error("failed to save user", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	verificationCode, err := verification.NewVerificationCode()
	if err != nil {
		log.Error("failed to generate verification code", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	linkID, err := a.linkProvider.CreateRegisterLink(ctx, id, verificationCode, a.verification.LinkTTL)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			log.Warn("user not found", sl.Err(err))

			return 0, fmt.Errorf("%s: %w", op, ErrInvalidUserID)
		}

		log.Error("failed to create register link", sl.Err(err))

		_ = a.userProvider.DeleteUser(ctx, email)

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	if id != 0 && linkID != 0 {
		sender := mailer.NewSender(a.mailer.FromEmail, a.mailer.Password, a.mailer.Server, int(a.mailer.Port))

		err := sender.SendConfirmRegisterMessage(email, id, route, verificationCode)
		if err != nil {
			log.Error("failed to send message", sl.Err(err))

			return 0, fmt.Errorf("%s: %w", op, err)
		}
	}

	log.Info("user registered")

	return id, err
}

// ConfirmRegister makes account status as confirmed
func (a *Auth) ConfirmRegister(ctx context.Context, userID int64, verificationToken string) error {
	const op = "auth.ConfirmRegister"

	log := a.log.With(slog.String("op", op))

	log.Info("confirming registration")

	link, err := a.linkProvider.RegisterLink(ctx, verificationToken)
	if err != nil {
		if errors.Is(err, storage.ErrRegLinkNotFound) {
			user, err := a.userProvider.UserByID(ctx, userID)
			if err != nil {
				if errors.Is(err, storage.ErrUserNotFound) {
					log.Warn("user not found", sl.Err(err))

					return fmt.Errorf("%s: %w", op, ErrInvalidUserID)
				}

				log.Error("failed to verify user", sl.Err(err))

				return fmt.Errorf("%s: %w", op, err)
			}

			if user.Verified {
				log.Warn("user already verified", ErrUserVerified)

				return fmt.Errorf("%s: %w", op, ErrUserVerified)
			}

			return fmt.Errorf("%s: %w", op, err)
		}

		log.Error("failed to get register link", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	fmt.Println(time.Now().UTC(), link.ExpiresAt.UTC())
	if time.Now().UTC().After(link.ExpiresAt.UTC()) {
		log.Warn("expired link", ErrExpiredLink)

		return fmt.Errorf("%s: %w", op, ErrExpiredLink)
	}

	err = a.userProvider.VerifyUser(ctx, link.UserID)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			log.Warn("user not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidUserID)
		}

		log.Error("failed to verify user", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	err = a.linkProvider.DeleteRegisterLink(ctx, link.ID)
	if err != nil {
		log.Error("failed to delete register link", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("user verified", slog.Int64("user_id", link.UserID))

	return nil
}

func (a *Auth) RefreshRegisterLink(ctx context.Context, email, route string) error {
	const op = "auth.RefreshRegisterLink"

	log := a.log.With(slog.String("op", op))

	log.Info("refreshing register link")

	user, err := a.userProvider.User(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(ErrInvalidCredentials))

			return fmt.Errorf("%s: %w", op, ErrInvalidCredentials)
		}

		a.log.Error("failed to get user", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	if user.Verified {
		a.log.Warn("user already exists", ErrUserExists)

		return fmt.Errorf("%s: %w", op, ErrUserExists)
	}

	verificationCode, err := verification.NewVerificationCode()
	if err != nil {
		log.Error("failed to refresh registration link", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	err = a.linkProvider.RefreshRegisterLink(ctx, user.ID, verificationCode, a.verification.LinkTTL)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			log.Warn("user not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidUserID)
		}

		log.Error("failed to refresh registration link", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	sender := mailer.NewSender(a.mailer.FromEmail, a.mailer.Password, a.mailer.Server, int(a.mailer.Port))

	err = sender.SendConfirmRegisterMessage(email, user.ID, route, verificationCode)
	if err != nil {
		log.Error("failed to send message", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

// IsAdmin checks if user is admin.
func (a *Auth) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	const op = "auth.IsAdmin"

	log := a.log.With(slog.String("op", op), slog.Int64("user_id", userID))

	log.Info("checking if user is admin")

	isAdmin, err := a.userProvider.IsAdmin(ctx, userID)
	if err != nil {
		log.Error("failed to check if user is admin", sl.Err(err))

		return false, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("checked if user is admin", slog.Bool("is_admin", isAdmin))

	return isAdmin, nil
}

func (a *Auth) SetUserAdmin(ctx context.Context, userID int64) error {
	const op = "auth.SetUserAdmin"

	log := a.log.With(slog.String("op", op), slog.Int64("user_id", userID))

	log.Info("setting user role to admin")

	user, err := a.userProvider.UserByID(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidUserID)
		}

		log.Error("failed to get user", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	if !user.Verified {
		log.Error("failed to get user", sl.Err(ErrConfirmationRequired))

		return fmt.Errorf("%s: %w", op, ErrConfirmationRequired)
	}

	err = a.userProvider.SetAdmin(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidUserID)
		}

		log.Error("failed to set user role to admin", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

func (a *Auth) UnsetUserAdmin(ctx context.Context, userID int64) error {
	const op = "auth.UnsetUserAdmin"

	log := a.log.With(slog.String("op", op), slog.Int64("user_id", userID))

	log.Info("removing user from admins")

	err := a.userProvider.UnsetAdmin(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrAdminNotFound) {
			a.log.Warn("admin not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidUserID)
		}

		log.Error("failed to unset admin role", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}
