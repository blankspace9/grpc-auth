package models

import "time"

type Link struct {
	ID                int64
	VerificationToken string
	UserID            int64
	LinkType          string
	ExpiresAt         time.Time
}
