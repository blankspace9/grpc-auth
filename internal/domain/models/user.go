package models

import "time"

type User struct {
	ID              int64
	Email           string
	PassHash        []byte
	Verified        bool
	LoginAttempts   int
	LastFailedLogin *time.Time
	Locked          bool
	CreatedAt       time.Time
}
