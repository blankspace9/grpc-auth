package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/grpc-auth/internal/domain/models"
	"gitlab.com/blankspace9/grpc-auth/internal/storage"
)

type Storage struct {
	db *sql.DB
}

type PostgresConnectionInfo struct {
	Host     string
	Port     string
	Username string
	DBName   string
	SSLMode  string
	Password string
}

// New creates a new instance of the PostgreSQL storage.
func New(connectionInfo PostgresConnectionInfo) (*Storage, error) {
	const op = "storage.postgres.New"

	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s password=%s",
		connectionInfo.Host, connectionInfo.Port, connectionInfo.Username, connectionInfo.DBName, connectionInfo.SSLMode, connectionInfo.Password))
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return &Storage{db: db}, nil
}

func (s *Storage) SaveUser(ctx context.Context, email string, passHash []byte) (int64, error) {
	const op = "storage.postgres.SaveUser"

	stmt, err := s.db.Prepare("INSERT INTO users(email, pass_hash, verified, login_attempts, last_failed_login, locked, created_at) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, email, passHash, false, 0, nil, false, time.Now())

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrUserExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return uid, nil
}

func (s *Storage) User(ctx context.Context, email string) (models.User, error) {
	const op = "storage.postgres.User"

	stmt, err := s.db.Prepare("SELECT id, email, pass_hash, verified, login_attempts, last_failed_login, locked, created_at FROM users WHERE email=$1")
	if err != nil {
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, email)

	var user models.User
	err = row.Scan(&user.ID, &user.Email, &user.PassHash, &user.Verified, &user.LoginAttempts, &user.LastFailedLogin, &user.Locked, &user.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.User{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	return user, nil
}

func (s *Storage) UserByID(ctx context.Context, userID int64) (models.User, error) {
	const op = "storage.postgres.UserByID"

	stmt, err := s.db.Prepare("SELECT id, email, pass_hash, verified, created_at FROM users WHERE id=$1")
	if err != nil {
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, userID)

	var user models.User
	err = row.Scan(&user.ID, &user.Email, &user.PassHash, &user.Verified, &user.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.User{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	return user, nil
}

func (s *Storage) DeleteUser(ctx context.Context, email string) error {
	const op = "storage.postgres.DeleteUser"

	stmt, err := s.db.Prepare("DELETE FROM users WHERE email=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, email)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}

func (s *Storage) DeleteExpired(ctx context.Context, userTTL time.Duration) error {
	const op = "storage.postgres.DeleteExpired"

	stmt, err := s.db.Prepare("DELETE FROM users WHERE verified=$1 AND created_at<=$2")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	_, err = stmt.ExecContext(ctx, false, time.Now().Add(-userTTL))
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

func (s *Storage) VerifyUser(ctx context.Context, userID int64) error {
	const op = "storage.postgres.VerifyUser"

	stmt, err := s.db.Prepare("UPDATE users SET verified=$1 WHERE id=$2")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, true, userID)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}

func (s *Storage) IncreaseLoginAttempts(ctx context.Context, email string) error {
	const op = "storage.postgres.IncreaseLoginAttemtps"

	stmt, err := s.db.Prepare("UPDATE users SET login_attempts=login_attempts+$1 WHERE email=$2")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, 1, email)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}

func (s *Storage) ResetLoginAttempts(ctx context.Context, email string) error {
	const op = "storage.postgres.ResetLoginAttemtps"

	stmt, err := s.db.Prepare("UPDATE users SET login_attempts=$1 WHERE email=$2")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, 0, email)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}

func (s *Storage) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	const op = "storage.postgres.IsAdmin"

	stmt, err := s.db.Prepare("SELECT role FROM user_roles WHERE user_id=$1")
	if err != nil {
		return false, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, userID)

	var role string
	err = row.Scan(&role)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("%s: %w", op, err)
	}

	return true, nil
}

func (s *Storage) SetAdmin(ctx context.Context, userID int64) error {
	const op = "storage.postgres.SetAdmin"

	stmtInsert, err := s.db.Prepare("INSERT INTO user_roles(user_id, role, created_at) VALUES($1, $2, $3)")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	_, err = stmtInsert.ExecContext(ctx, userID, "admin", time.Now())
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" { // Already exists
			return nil
		} else if errors.As(err, &pgErr) && pgErr.Code == "23503" { // Foreign key violation
			return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

func (s *Storage) UnsetAdmin(ctx context.Context, userID int64) error {
	const op = "storage.postgres.UnsetAdmin"

	stmtInsert, err := s.db.Prepare("DELETE FROM user_roles WHERE user_id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	res, err := stmtInsert.ExecContext(ctx, userID)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrAdminNotFound)
	}

	return nil
}

func (s *Storage) RegisterLink(ctx context.Context, verificationToken string) (models.Link, error) {
	const op = "storage.postgres.RegisterLink"

	stmt, err := s.db.Prepare("SELECT id, verification_code, user_id, link_type, expires_at FROM one_time_links WHERE verification_code=$1 AND link_type=$2")
	if err != nil {
		return models.Link{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, verificationToken, "confirm_register")

	var link models.Link
	err = row.Scan(&link.ID, &link.VerificationToken, &link.UserID, &link.LinkType, &link.ExpiresAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Link{}, fmt.Errorf("%s: %w", op, storage.ErrRegLinkNotFound)
		}

		return models.Link{}, fmt.Errorf("%s: %w", op, err)
	}

	return link, nil
}

func (s *Storage) CreateRegisterLink(ctx context.Context, user_id int64, verificationToken string, linkTTL time.Duration) (int64, error) {
	const op = "storage.postgres.CreateRegisterLink"

	stmt, err := s.db.Prepare("INSERT INTO one_time_links(verification_code, user_id, link_type, expires_at) VALUES($1, $2, $3, $4) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, verificationToken, user_id, "confirm_register", time.Now().Add(linkTTL))

	var linkID int64
	err = row.Scan(&linkID)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) {
			switch pgErr.Code {
			case "23503": // Foreign key violation
				return 0, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
			case "23505": // Already exists
				return 0, fmt.Errorf("%s: %w", op, storage.ErrRegLinkAlreadyExists)
			}
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return linkID, nil
}

func (s *Storage) RefreshRegisterLink(ctx context.Context, userID int64, verificationToken string, linkTTL time.Duration) error {
	const op = "storage.postgres.RefreshRegisterLink"

	stmt, err := s.db.Prepare("UPDATE one_time_links SET verification_code=$1, expires_at=$2 WHERE user_id=$3")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, verificationToken, pq.FormatTimestamp(time.Now().Add(linkTTL)), userID)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}

func (s *Storage) DeleteRegisterLink(ctx context.Context, linkID int64) error {
	const op = "storage.postgres.DeleteRegisterLink"

	stmt, err := s.db.Prepare("DELETE FROM one_time_links WHERE id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, linkID)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrRegLinkNotFound)
	}

	return nil
}

func (s *Storage) App(ctx context.Context, appID int) (models.App, error) {
	const op = "storage.postgres.App"

	stmt, err := s.db.Prepare("SELECT id, name, secret FROM apps WHERE id=$1")
	if err != nil {
		return models.App{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, appID)

	var app models.App
	err = row.Scan(&app.ID, &app.Name, &app.Secret)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.App{}, fmt.Errorf("%s: %w", op, storage.ErrAppNotFound)
		}

		return models.App{}, fmt.Errorf("%s: %w", op, err)
	}

	return app, nil
}
