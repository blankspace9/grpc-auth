package storage

import "errors"

var (
	ErrUserExists    = errors.New("user already exists")
	ErrUserNotFound  = errors.New("user not found")
	ErrAdminNotFound = errors.New("admin not found")

	ErrRegLinkNotFound      = errors.New("registration link not found")
	ErrRegLinkAlreadyExists = errors.New("registration link already exists")

	ErrAppNotFound = errors.New("app not found")
)
