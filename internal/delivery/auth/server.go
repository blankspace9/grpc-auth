package auth

import (
	"context"
	"errors"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/blankspace9/grpc-auth/internal/lib/validation"
	"gitlab.com/blankspace9/grpc-auth/internal/services/auth"
	authv1 "gitlab.com/blankspace9/protos/gen/go/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	emptyValue = 0
)

// Interface of Auth service
type Auth interface {
	Login(ctx context.Context, email string, password string, appID int) (string, string, error)
	VerifyCaptcha(ctx context.Context, email, captchaResponse string) error

	RegisterNewUser(ctx context.Context, email, password, route string) (userID int64, err error)
	ConfirmRegister(ctx context.Context, userID int64, token string) error
	RefreshRegisterLink(ctx context.Context, email, route string) error

	IsAdmin(ctx context.Context, userID int64) (bool, error)
	SetUserAdmin(ctx context.Context, userID int64) error
	UnsetUserAdmin(ctx context.Context, userID int64) error
}

type serverAPI struct {
	authv1.UnimplementedAuthServer
	auth Auth
}

func RegisterServerAPI(gRPC *grpc.Server, auth Auth) {
	authv1.RegisterAuthServer(gRPC, &serverAPI{auth: auth})
}

func (s *serverAPI) Login(ctx context.Context, req *authv1.LoginRequest) (*authv1.LoginResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}

	if req.GetPassword() == "" {
		return nil, status.Error(codes.InvalidArgument, "password required")
	}

	if req.GetAppId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid app_id")
	}

	accessToken, _, err := s.auth.Login(ctx, req.GetEmail(), req.GetPassword(), int(req.GetAppId()))
	if err != nil {
		if errors.Is(err, auth.ErrInvalidCredentials) {
			return nil, status.Error(codes.InvalidArgument, "invalid credentials")
		} else if errors.Is(err, auth.ErrCaptchaRequired) {
			return nil, status.Error(codes.Unavailable, "captcha required")
		} else if errors.Is(err, auth.ErrConfirmationRequired) {
			return nil, status.Error(codes.FailedPrecondition, "confirmation required")
		} else if errors.Is(err, auth.ErrInvalidAppID) {
			return nil, status.Error(codes.NotFound, "app not found")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return &authv1.LoginResponse{
		Token: accessToken,
	}, nil
}

func (s *serverAPI) VerifyCaptcha(ctx context.Context, req *authv1.VerifyCaptchaRequest) (*emptypb.Empty, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}
	if req.GetCaptchaResponse() == "" {
		return nil, status.Error(codes.InvalidArgument, "captcha response required")
	}

	err := s.auth.VerifyCaptcha(ctx, req.GetEmail(), req.GetCaptchaResponse())
	if err != nil {
		if errors.Is(err, auth.ErrCaptchaVerificationFailed) {
			return nil, status.Error(codes.InvalidArgument, "invalid captcha verification")
		} else if errors.Is(err, auth.ErrInvalidCredentials) {
			return nil, status.Error(codes.NotFound, "user not found")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return nil, nil
}

func (s *serverAPI) Register(ctx context.Context, req *authv1.RegisterRequest) (*authv1.RegisterResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}

	if !validation.ValidatePassword(req.GetPassword()) {
		return nil, status.Error(codes.InvalidArgument, "invalid password")
	}

	if !valid.IsURL(req.GetRoute()) {
		return nil, status.Error(codes.InvalidArgument, "invalid route")
	}

	userID, err := s.auth.RegisterNewUser(ctx, req.GetEmail(), req.GetPassword(), req.GetRoute())
	if err != nil {
		if errors.Is(err, auth.ErrUserExists) {
			return nil, status.Error(codes.AlreadyExists, "user already exists")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return &authv1.RegisterResponse{
		UserId: userID,
	}, nil
}

func (s *serverAPI) ConfirmRegister(ctx context.Context, req *authv1.ConfirmRegisterRequest) (*emptypb.Empty, error) {
	if req.GetVerificationToken() == "" {
		return nil, status.Error(codes.InvalidArgument, "verification token is required")
	}

	if req.GetUserId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid user_id")
	}

	err := s.auth.ConfirmRegister(ctx, req.GetUserId(), req.GetVerificationToken())
	if err != nil {
		if errors.Is(err, auth.ErrInvalidUserID) {
			return nil, status.Error(codes.NotFound, "user not found")
		} else if errors.Is(err, auth.ErrExpiredLink) {
			return nil, status.Error(codes.DeadlineExceeded, "expired link")
		} else if errors.Is(err, auth.ErrUserVerified) {
			return nil, status.Error(codes.AlreadyExists, "user already verified")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return nil, nil
}

func (s *serverAPI) RefreshRegisterLink(ctx context.Context, req *authv1.RefreshLinkRequest) (*emptypb.Empty, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "email is required")
	}

	if !valid.IsURL(req.GetRoute()) {
		return nil, status.Error(codes.InvalidArgument, "route is required")
	}

	err := s.auth.RefreshRegisterLink(ctx, req.GetEmail(), req.GetRoute())
	if err != nil {
		if errors.Is(err, auth.ErrInvalidUserID) {
			return nil, status.Error(codes.NotFound, "user not found")
		} else if errors.Is(err, auth.ErrUserExists) {
			return nil, status.Error(codes.AlreadyExists, "user already exists")
		} else if errors.Is(err, auth.ErrInvalidCredentials) {
			return nil, status.Error(codes.InvalidArgument, "invalid credentials")
		}
		return nil, status.Error(codes.Internal, "internal error")
	}

	return nil, nil
}

func (s *serverAPI) IsAdmin(ctx context.Context, req *authv1.IsAdminRequest) (*authv1.IsAdminResponse, error) {
	if req.GetUserId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid user_id")
	}

	isAdmin, err := s.auth.IsAdmin(ctx, req.GetUserId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &authv1.IsAdminResponse{
		IsAdmin: isAdmin,
	}, nil
}

func (s *serverAPI) SetAdmin(ctx context.Context, req *authv1.SetUserRoleRequest) (*emptypb.Empty, error) {
	if req.GetUserId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid user_id")
	}

	err := s.auth.SetUserAdmin(ctx, req.GetUserId())
	if err != nil {
		if errors.Is(err, auth.ErrInvalidUserID) {
			return nil, status.Error(codes.NotFound, "user not found")
		} else if errors.Is(err, auth.ErrConfirmationRequired) {
			return nil, status.Error(codes.Canceled, "user is not verified")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return nil, nil
}

func (s *serverAPI) UnsetAdmin(ctx context.Context, req *authv1.SetUserRoleRequest) (*emptypb.Empty, error) {
	if req.GetUserId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid user_id")
	}

	if req.GetRole() == "" {
		return nil, status.Error(codes.InvalidArgument, "role is required")
	}

	err := s.auth.UnsetUserAdmin(ctx, req.GetUserId())
	if err != nil {
		if errors.Is(err, auth.ErrInvalidUserID) {
			return nil, status.Error(codes.NotFound, "admin not found")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return nil, nil
}
