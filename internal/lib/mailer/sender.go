package mailer

import (
	"fmt"
	"net/url"
	"strconv"

	"gopkg.in/gomail.v2"
)

type EmailSender struct {
	dialer *gomail.Dialer
}

func NewSender(from, password, host string, port int) *EmailSender {
	return &EmailSender{
		dialer: gomail.NewDialer(host, port, from, password),
	}
}

func (es *EmailSender) SendConfirmRegisterMessage(to string, userID int64, route, verificationCode string) error {
	params := url.Values{}
	params.Add("user_id", strconv.Itoa(int(userID)))
	params.Add("token", verificationCode)

	u, err := url.Parse(route)
	if err != nil {
		return err
	}
	u.RawQuery = params.Encode()

	content := fmt.Sprintf("\nHello!\nFollow the link to complete registration: %s\n\nGood Luck!", u.String())

	message := gomail.NewMessage()
	message.SetHeader("From", es.dialer.Username)
	message.SetHeader("To", to)
	message.SetHeader("Subject", "Your account verification code")
	message.SetBody("text/plain", content)

	if err := es.dialer.DialAndSend(message); err != nil {
		return err
	}

	return nil
}
