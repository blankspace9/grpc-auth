package jwt

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/blankspace9/grpc-auth/internal/domain/models"
)

func NewTokens(user models.User, app models.App, userRoles []string, duration time.Duration) (string, string, error) {
	accessToken := jwt.New(jwt.SigningMethodHS256)

	claims := accessToken.Claims.(jwt.MapClaims)
	claims["sub"] = user.ID
	claims["exp"] = time.Now().Add(duration).Unix()
	claims["role"] = userRoles
	claims["app_id"] = app.ID

	accessTokenString, err := accessToken.SignedString([]byte(app.Secret))
	if err != nil {
		return "", "", err
	}

	refreshToken := uuid.New().String()

	return accessTokenString, refreshToken, nil
}
