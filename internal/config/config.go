package config

import (
	"flag"
	"os"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type (
	Config struct {
		Env          string             `yaml:"env" env-default:"local"`
		GRPC         GRPCConfig         `yaml:"grpc"`
		Tokens       Tokens             `yaml:"tokens" env-required:"true"`
		Login        Login              `yaml:"login" env-required:"true"`
		Verification VerificationConfig `yaml:"verification" env-required:"true"`
		Storage      Postgres
		EmailSender  EmailSender
	}

	AppConfig struct {
		GRPC         GRPCConfig         `yaml:"grpc"`
		Tokens       Tokens             `yaml:"token_ttl" env-required:"true"`
		Login        Login              `yaml:"login" env-required:"true"`
		Verification VerificationConfig `yaml:"verification" env-required:"true"`
		Storage      Postgres
		EmailSender  EmailSender
	}

	MigrateConfig struct {
		DB Postgres
	}

	GRPCConfig struct {
		Port    int           `yaml:"port"`
		Timeout time.Duration `yaml:"timeout"`
	}

	Tokens struct {
		AccessTokenTTL  time.Duration `yaml:"access_token_ttl" env-required:"true"`
		RefreshTokenTTL time.Duration `yaml:"refresh_token_ttl" env-required:"true"`
	}

	Login struct {
		MaxAttempts       int           `yaml:"max_attempts" env-required:"true"`
		LockDuration      time.Duration `yaml:"lock_duration" env-required:"true"`
		CaptchaKey        string        `env:"CAPTCHA_KEY"`
		CaptchaServerPath string        `env:"CAPTCHA_SERVER_PATH"`
	}

	VerificationConfig struct {
		LinkTTL       time.Duration `yaml:"link_ttl"`
		CheckInterval time.Duration `yaml:"check_interval"`
		UserTTL       time.Duration `yaml:"user_ttl"`
	}

	Postgres struct {
		Host     string `env:"HOST" env-default:"localhost"`
		Port     string `env:"PORT" env-default:"5432"`
		Username string `env:"USER" env-default:"postgres"`
		DBName   string `env:"NAME"`
		SSLMode  string `env:"SSLMODE" env-default:"disable"`
		Password string `env:"PASSWORD"`
	}

	EmailSender struct {
		FromEmail string `env:"EMAIL_FROM"`
		Password  string `env:"EMAIL_PASSWORD"`
		Server    string `env:"SMTP_SERVER" env-default:"smtp.gmail.com"`
		Port      int    `env:"SMTP_PORT" env-default:"587"`
	}
)

func MustLoad() *Config {
	path := fetchConfigPath()
	if path == "" {
		panic("config path is empty")
	}

	return MustLoadByPath(path, ".env")
}

func MustLoadByPath(configPath string, envPath string) *Config {
	// check if file exists
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		panic("config file does not exists: " + configPath)
	}

	var cfg Config

	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		panic("failed to read config: " + err.Error())
	}

	if err := godotenv.Load(envPath); err != nil {
		panic("failed to load .env file: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg.Storage); err != nil {
		panic("failed to read config: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg.EmailSender); err != nil {
		panic("failed to read config: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg.Login); err != nil {
		panic("failed to read config: " + err.Error())
	}

	return &cfg
}

func MigrateMustLoad() *MigrateConfig {
	cfg := new(MigrateConfig)

	if err := godotenv.Load(".env"); err != nil {
		panic("failed to load .env file: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg.DB); err != nil {
		panic("failed to read config: " + err.Error())
	}

	return cfg
}

// flag > env > default
func fetchConfigPath() string {
	var res string

	flag.StringVar(&res, "config", "", "path to config file")
	flag.Parse()

	if res == "" {
		res = os.Getenv("CONFIG_PATH")
	}

	return res
}
