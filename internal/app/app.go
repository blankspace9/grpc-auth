package app

import (
	"log/slog"

	grpcapp "gitlab.com/blankspace9/grpc-auth/internal/app/grpc"
	"gitlab.com/blankspace9/grpc-auth/internal/app/usercleaner"
	"gitlab.com/blankspace9/grpc-auth/internal/config"
	"gitlab.com/blankspace9/grpc-auth/internal/services/auth"
	"gitlab.com/blankspace9/grpc-auth/internal/services/usermanager"
	"gitlab.com/blankspace9/grpc-auth/internal/storage/postgres"
)

type App struct {
	GRPCServer  *grpcapp.App
	UserCleaner *usercleaner.App
}

func New(log *slog.Logger, cfg config.AppConfig) *App {
	storage, err := postgres.New(postgres.PostgresConnectionInfo(cfg.Storage))
	if err != nil {
		panic(err)
	}

	authService := auth.New(log, storage, storage, storage, cfg.Tokens, cfg.Verification, cfg.EmailSender, cfg.Login)

	grpcApp := grpcapp.New(log, authService, cfg.GRPC.Port)

	userManagerService := usermanager.New(log, storage, cfg.Verification.CheckInterval, cfg.Verification.UserTTL)

	userCleanerApp := usercleaner.New(log, *userManagerService)

	return &App{
		GRPCServer:  grpcApp,
		UserCleaner: userCleanerApp,
	}
}
