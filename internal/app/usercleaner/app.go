package usercleaner

import (
	"log/slog"

	"gitlab.com/blankspace9/grpc-auth/internal/services/usermanager"
)

type App struct {
	log     *slog.Logger
	service usermanager.Manager
}

func New(logger *slog.Logger, userManager usermanager.Manager) *App {
	return &App{
		log:     logger,
		service: userManager,
	}
}

func (a *App) Run() {
	const op = "regmanagerapp.Run"

	log := a.log.With(slog.String("op", op))

	log.Info("User Cleaner is running")
	a.service.Start()
}

func (a *App) Stop() {
	const op = "regmanagerapp.Stop"

	a.log.With(slog.String("op", op)).Info("stopping user cleaner service")

	a.service.Stop()
}
